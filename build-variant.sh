#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

arch=$1

docker build --pull \
	-t shutdownd:latest-${arch} \
	--build-arg BASE_IMAGE=quay.io/ethanwu10/${arch}-ros \
	.
