ARG BASE_IMAGE=quay.io/ethanwu10/amd64-ros
FROM ${BASE_IMAGE}:kinetic-ros-base

RUN pip install pipenv

RUN mkdir -p /app
WORKDIR /app
COPY Pipfile Pipfile.lock ./

RUN pipenv install --two --system --deploy

COPY . .

ENTRYPOINT ["/ros_entrypoint.sh", "python", "main.py"]
CMD []
