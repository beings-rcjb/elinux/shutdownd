from requests import post
from requests.auth import HTTPBasicAuth
import rospy
from std_msgs.msg import UInt8MultiArray
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin


AUTH = HTTPBasicAuth('root', 'pass')
SERVER = None


def rx_packet(packet):
    data = bytes(packet.data)
    if data == b'\x0f':
        post(urljoin(SERVER, '/power'), json={'type': 'poweroff'}, auth=AUTH)


if __name__ == '__main__':
    from configargparse import ArgumentParser

    parser = ArgumentParser(description="Shutdown daemon")
    parser.add_argument('-s', '--server', env_var='SERVER',
                        help="Host nanoctld URI")
    parser.add_argument('-i', '--id', type=int,
                        default=1, env_var='ID',
                        help="Protocol ID")
    parser.add_argument('-I', '--iface-id', type=int,
                        default=0, env_var='IFACE_ID',
                        help="Interface ID")
    args = parser.parse_args()

    rospy.init_node('shutdownd', anonymous=True)

    address = '/'.join(['', 'serial' + str(args.iface_id), str(args.id), 'rx'])
    SERVER = args.server
    rospy.Subscriber(address, UInt8MultiArray, rx_packet)

    rospy.spin()
